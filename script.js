console.log('JS Loops');

/*
	Repetition Control Structure

	Loops
		- way to do something repeatedly


		// for loop
		// while loop
		// do-while loop
*/
	/*
		For Loop
			- more flexible
			- consists of 3 parts:

				1. initial value
				2. condition
				3. iteration ++ or --

		Syntax:

			for(initial; condition; iteration){
				//statement or for loop codeblock
			}
	

	//Example:
		// Go 5 steps to the east starting from 0.

		for(let x = 0; x < 5; x++){

			console.log(`Walking east one step:`, x);
		}

	*/

	/*
		Mini Activity

		Create a loop that will count starting from 20 down to zero

		Display "Skip counting by <count>"
		
		screenshot code and output, send to gc

	
		for(let y = 20; y >= 0; y--){
			console.log(`Skip counting by`, y)
		}
	*/

	/*
		Continue and Break

			Continue
				- skip the current loop and proceed to the next iteration

			Break
				- ends the execution of the loop / exits the loop
	

		for(let a = 1; a < 20; a++){

			if(a % 2 === 0){
				continue;
			}

			// console.log(a);

			if(a > 10){
				break;
			}

			console.log(a);
		}
	*/

//Q: Can we use loop in a string? - Y

let myName = `Christiana Joy Pague-Reid`;
console.log( myName.length );	//count starts at 1
console.log( myName[13] );		//count starts at 0

// length property
	// returns the count/number of its reference variable

//index
	//in JS, index is zero based
	//referring to the position of each character/element
/*
	let i = 0;

	for(i; i < myName.length; i++){

		if(myName[i] == "i"){
			continue;
		}
		if(myName[i] == "a"){
			break;
		}

		console.log( myName[i] );
	}
*/
	//refactor our current code, that if the charcter is equivalent to i, skip the current loop, if character is equivalent to a, exit the loop


/* Nested Loops
	
	//Outer loop
	for(let x = 0; x <= 5; x++){
		console.log(x);

		//Inner loop
		for(let y = 0; y <= 5; y++){

			console.log(`${x} + ${y} = ${x + y}`)
		}
	}
*/


	/*
		While loop
			- loop will run only when the condition is true

		syntax:

			while(condition){

				// iteration ++ or --
			}
	
		let count = 1;

		while(count <= 5){
			console.log(count)
			
			count++
		}
	

			// Display 1 - 5 in the console

			let x = 1;

			while(x <= 5){
				console.log(x)

				x++
			}
	*/


	/*
		do-while loop
			- guaranteed do code block would run at least once before checking the condition

		syntax:

			do{
				//statement

			} while()
	*/

	let step = 20;

	do {
		console.log(step)
		step--

	} while( step <= 0 )


	//using while loop
	while(step <= 0){
		console.log(step)
		step--
	}